<!DOCTYPE html>
<html>
<head>
	<title>animal</title>
</head>
<body>
	<?php
	/**
	 * 
	 */
	class animal
	{
		public $name;
		public $legs = 2;
		public $cold_blooded = "false";

		function __construct ($nama)
		{
			$this -> name=$nama;
		}

		function getName()
		{
			return $this->name;
		}

		function getLeg()
		{
			return $this -> legs;
		}

		function get_cold_blooded()
		{
			return $this -> cold_blooded;
		}
		
	}

	/**
	 * 
	 */
	class ape extends animal
	{
		

		public function getYell()
		{
			echo "Auoo";
			echo "<br>"; 
		}
	}

	/**
	 * 
	 */
	class frog extends animal
	{
		
		public function getJump()
		{
			echo "hop hop";
		}
	}
	?>

</body>
</html>